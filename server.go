package main

import (
	"fmt"
	"net/http"
	"log"
	"io"
	"time"
)

func main() {
	http.HandleFunc("/go/hello", helloHandler)
	http.HandleFunc("/go/time", timeHandler)

	if err := http.ListenAndServe(":3333", nil); err != nil {
        log.Fatal(err)
    }
}

func helloHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("Hello path called")
	io.WriteString(w, "hello Go server")
}

func timeHandler(w http.ResponseWriter, r *http.Request) {
	log.Print("Time path called")
	tm := time.Now()
	s := fmt.Sprintf("Date: %v\nTime: %v\n", tm.Format("Jan 2, 2006"), tm.Format("15:04:05"))
	io.WriteString(w, s)
}